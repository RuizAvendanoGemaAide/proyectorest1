<%-- 
    Document   : documentacion
    Created on : 8/08/2014, 10:06:39 AM
    Author     : miguel
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-3.2.0/bootstrap.min.css" />">
        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-3.2.0/bootstrap-theme.min.css" />">
        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-3.2.0/main.css" />">
        <script src="<c:url value="/resources/js/modernizr-2.6.2-respond-1.1.0.min.js" />"  type="text/javascript"></script> 
        <script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"  type="text/javascript"></script> 
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		
		<!--contenido -->
		
		<!-- /contenido -->
		
        <div class="blog-masthead">
          <div class="container">
            <nav class="blog-nav">
              <a class="blog-nav-item active" href="#">Inicio</a>
              <a class="blog-nav-item" href="#">Acerca de</a>
            </nav>
          </div>
        </div>

        <div class="container">

          <div class="blog-header">

            <h1 class="blog-title">API INEGI</h1>

            <p class="lead blog-description">Encuesta nacional de ocupación y empleo</p>
          </div>

          <div class="row">

            <div class="col-sm-8 blog-main">

              <div class="blog-post">
                <h2 class="blog-post-title" id="Recursos">Recursos</h2>

                <!--ENTIDADES-->
                <h2 id="Entidades">Entidades</h2>
                 <p>Las Entidades son los estados de la República Mexicana. Cada Entidad tiene un ID y una Descripción. El ID es un valor numérico único, la Descripción contiene el nombre del Estado(Entidad).</p>
                <p>Las operaciones que se pueden hacer sobre el recurso Entidades son GET, POST, PUT y DELETE. Cada método tiene sus parámetros los cuales se describen a continuación.</p>
                
                <h3>GET</h3>
                <p>Hacer una petición al siguiente recurso devuelve un XML o JSON, según se especifique dentro de las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con la lista de id y descripción de todas las entidades registradas.</p>

                <pre><code>curl -X GET \-H "accept: application/json" \dominio/api/entidades<br />curl -X GET \-H "accept: application/xml" \dominio/api/entidades</code></pre>

                <p>Resultado en XML</p>

                <pre><code>&ltlist&gt<br />   &ltentidad&gt<br />       &ltidEntidad&gt0&lt/idEntidad&gt<br />       &ltdescEntidad&gtNacional&lt/descEntidad&gt<br />  &lt/entidad&gt<br />   &ltentidad&gt<br />       &ltidEntidad&gt1&lt/idEntidad&gt<br />       &ltdescEntidad&gtOaxaca&lt/descEntidad&gt<br />  &lt/entidad&gt<br />&lt/list&gt</code></pre>
                
                <p>Resultado en JSON</p>

                <pre><code>{<br />  "list" : [<br />       {<br />         "idEntidad" : 0, <br />         "descEntidad" : "Nacional"<br/>},<br/>       {<br />         "idEntidad" : 1, <br />         "descEntidad" : "Oaxaca"<br/>       }<br/>  ]<br />}</code></pre>
                
                <p>Para obtener una Entidad en específico, se agrega el id de la Entidad.</p>
                <pre><code>curl -X GET \-H "accept: application/json" \dominio/api/entidades/0<br />curl -X GET \-H "accept: application/xml" \dominio/api/entidades/0</code></pre>
                <p>Esta solicitud regresara un XML o JSON, según se especifique en las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con el id y la descripción de la Entidad. </p>
                <pre><code>&ltentidad&gt<br />  &ltidEntidad&gt0&lt/idEntidad&gt<br />  &ltdescEntidad&gtNacional&lt/descEntidad&gt<br />&lt/entidad&gt</code></pre>
                <pre><code>{<br />  "entidad" : {<br />     "idEntidad" : 0, <br />     "descEntidad" : "Nacional"<br/>       }<br />}</code></pre>
                <p>En caso de que no existan los recursos solicitados, se regresa un mensaje de warning</p>
                <pre><code>&ltmessage&gt<br />  &lttype&gtwarning&lt/type&gt<br />  &ltdescription&gtEl elemento no existe&lt/description&gt<br />&lt/message&gt</code></pre>
                <pre><code>{<br />  "message" : {<br />     "type" : "warning, <br />     "description" : "El elemento seleccionado no existe"<br/>       }<br />}</code></pre>
                <h3>POST</h3>
                <p>Para registrar una nueva Entidad, se da solo el nombre de la nueva Entidad y se puede enviar en XML o JSON. El servidor responde con un mensaje en el mismo formato en que la solicitud fue hecha. Se realiza la petición a la siguiente URL:</p>
                <pre><code>curl -X POST \-H "accept: application/json" \dominio/api/entidades<br />curl -X POST \-H "accept: application/xml" \dominio/api/entidades</code></pre>
                <p>En caso de éxito, se regresará un mensaje de éxito. De lo contrario un mensaje de warning.</p>
                <h3>PUT</h3>
                <p>Para modificar alguna Entidad existente, se realiza la solicitud a la siguiente URL mostrada en el recuadro, indicando el id de la entidad a modificar. Se pueden enviar formatos XML o JSON.</p>
                <pre><code>curl -X PUT \-H "accept: application/json" \dominio/api/entidades/0<br />curl -X GET \-H "accept: application/xml" \dominio/api/entidades/0</code></pre>
                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning.</p>
                <h3>DELETE</h3>
                <p>Para eliminar una Entidad, se indica el id de la Entidad, muy muy similar a la solicitud GET.</p>
                <pre><code>curl -X DELETE \-H "accept: application/json" \dominio/api/entidades/0<br />curl -X GET \-H "accept: application/xml" \dominio/api/entidades/0</code></pre>
                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning</p>
                

                <!--MUNICIPIOS-->
                <a id="Municipios"></a><h2>Municipios</h2>
                <p>Los Los municipios, como su nombre lo indica, son los municipios  pertenecientes a cada uno de los estados de la República Mexicana, cada estado tiene un numero determinado de municipios. Cada municipio tiene un ID y una descripción, además del ID de la entidad a la que pertenece</p>

                <p>Los Los municipios, como su nombre lo indica, son los municipios  pertenecientes a cada uno de los estados de la República Mexicana, cada estado tiene un numero determinado de municipios. Cada municipio tiene un ID y una descripción, además del ID de la entidad a la que pertenece</p>
                <h3>GET</h3>
                <p>Hacer una petición al siguiente recurso devuelve un XML o JSON, según se especifique dentro de las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con la lista de id y descripción de todas los Municipios registrados.</p>

                <pre><code>curl -X GET \-H "accept: application/json" \dominio/api/municipios<br />curl -X GET \-H "accept: application/xml" \dominio/api/municipios</code></pre>

                <p>Resultado en XML</p>

                <pre><code>&ltlist&gt<br />   &ltmunicipio&gt<br />       &ltidMunicipio&gt0&lt/idMunicipio&gt<br />       &ltdescMunicipio&gtNacional&lt/descMunicipio&gt<br />       &ltidEntidad&gt0&lt/idEntidad&gt<br />  &lt/municipio&gt<br />&lt/list&gt</code></pre>
                
                <p>Resultado en JSON</p>

                <pre><code>{<br />  "data" : [<br />       {<br />         "idMunicipio" : 0, <br />         "descMunicipio" : "Nacional", <br />         "idEntidad" : 0<br/>}<br/>  ]<br />}</code></pre>
                
                <p>Para obtener una Municipio en específico, se agrega el id del Municipio.</p>

                <pre><code>curl -X GET \-H "accept: application/json" \dominio/api/municipios/{id}<br />curl -X GET \-H "accept: application/xml" \dominio/api/municipios/{id}</code></pre>
                
                <p>Esta solicitud regresara un XML o JSON, según se especifique en las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con el id y la descripción del Municipio. </p>
                
                <pre><code>&ltmunicipio&gt<br />  &ltidMunicipio&gt0&lt/idMunicipio&gt<br />  &ltdescMunicipio&gtNacional&lt/descMunicipio&gt<br />       &ltidEntidad&gt0&lt/idEntidad&gt<br />&lt/municipio&gt</code></pre>
                
                <pre><code>{<br />  "municipio" : {<br />     "idMunicipio" : 0, <br />     "descMunicipio" : "Nacional", <br />         "idEntidad" : 0<br/>       }<br />}</code></pre>
                
                <p>En caso de que no existan los recursos solicitados, se regresa un mensaje de warning</p>
                
                <pre><code>&ltmessage&gt<br />  &lttype&gtwarning&lt/type&gt<br />  &ltdescription&gtEl elemento no existe&lt/description&gt<br />&lt/message&gt</code></pre>
                
                <pre><code>{<br />  "message" : {<br />     "type" : warning, <br />     "description" : "El elemento selecionado no existe."<br/>       }<br />}</code></pre>
                
                <h3>POST</h3>
                
                <p>Para registrar una nueva Entidad, se da solo el nombre de la nueva Entidad y se puede enviar en XML o JSON. El servidor 
                responde con un mensaje en el mismo formato en que la solicitud fue hecha. Se realiza la petición a la siguiente URL:</p>
                
                <pre><code>curl -X POST \-H "accept: application/json" \dominio/api/municipios<br />curl -X POST \-H "accept: application/xml" \dominio/api/municipios</code></pre>
                
                <p>En caso de éxito, se regresará un mensaje de éxito. De lo contrario un mensaje de warning.</p>
                
                <h3>PUT</h3>
                
                <p>Para modificar alguna Entidad existente, se realiza la solicitud a la siguiente URL mostrada en el recuadro, indicando el 
                id de la entidad a modificar. Se pueden enviar formatos XML o JSON.</p>
                
                <pre><code>curl -X PUT \-H "accept: application/json" \dominio/api/municipios/0<br />curl -X PUT \-H "accept: application/xml" \dominio/api/municipios/0</code></pre>
                
                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning.</p>
                
                <h3>DELETE</h3>
                
                <p>Para eliminar una Entidad, se indica el id de la Entidad, muy muy similar a la solicitud GET.</p>
                
                <pre><code>curl -X DELETE \-H "accept: application/json" \dominio/api/municipios/0<br />curl -X DELETE \-H "accept: application/xml" \dominio/api/municipios/0</code></pre>
                
                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning</p>



                <!--TEMAS 1-->
                <a id="Temas1"></a><h2>Temas de Nivel 1</h2>
                <p>Los "Temas de nivel 1" son los temas principales que indican una clasificación de los datos, a cada tema le corresponde un ID</p>

                <h3>GET</h3>
                <p>Hacer una petición a este  recurso devuelve un XML o JSON, según se específique dentro de las cabeceras, con la lista de id y descripción de todas los temas registrados.</p>
                <pre><code>GET dominio/api/temasNivel1</code></pre>
                <p>XML</p>
                <pre><code>&ltlist&gt<br />  &ltTemasNivel1&gt<br />       &ltidTemasNivel1&gt0&lt/idTemasNivel1&gt<br />&lt/desctemaNivel1&gtSociedad y Gobierno&lt/desctemaNivel1&gt<br />  &lt/TemasNivel1&gt<br /> &lt/list&gt<br /></code> </pre>
                <p>JSON</p>
                <pre><code>{<br />  "list" : [<br />       {<br />         "idtemasNivel1" : 0, <br />         "desctemasNivel1" : "Sociedad y Gobierno"<br/>  }<br/>  ]<br />}</code></pre>
                <p>Para obtener una tema de nivel uno especifico solo se le agrega un id.</p>
                <pre><code>GET dominio/api/temasNivel1/0</code></pre>
                <p>XML</p>
                <pre><code>&ltlist&gt<br />  &ltTemasNivel1&gt<br />       &ltidTemasNivel1&gt0&lt/idTemasNivel1&gt<br />&lt/desctemaNivel1&gtSociedad y Gobierno&lt/desctemaNivel1&gt<br />  &lt/TemasNivel1&gt<br /> &lt/list&gt<br /></code> </pre>
                <p>JSON</p>
                <pre><code>{<br />  "list" : [<br />       {<br />         "idtemasNivel1" : 0, <br />         "desctemasNivel1" : "Sociedad y Gobierno"<br/>  }<br/>  ]<br />}</code></pre>
                <p>En caso de que no existan los recursos solicitados, se regresa un mensaje de warning</p>
                <p>XML</p>
                <pre><code>&ltmessage&gt<br />  &lttype&gtwarning&lt/type&gt<br />  &ltdescription&gtEl elemento no existe&lt/description&gt<br />&lt/message&gt</code></pre>
                <p>JSON</p>
                <pre><code>{<br />  "message" : {<br />     "type" : "warning", <br />     "description" : "El elemento no existe"<br/>       }<br />}</code></pre>
                <h3>POST</h3>
                <p>Para registrar un nuevo tema de nivel 1, se garega el nombre de un nuevo tema de nivel 1 y se puede enviar en XML o JSON. El servidor responde con un mensaje en el mismo formato en que la solicitud fue hecha. Se realiza la petición a la siguiente URL:</p>
                <p>Peticion XML</p>
                <pre><code>curl -X POST \<br>-H "Content-Type: application/xml" \<br>-H "accept: application/json" \<br>-d '{"descripcion":"Sociedad"}' \<br>http://localhost:8080/api/temasnivel1</code></pre>

                <p>Peticion JSON</p>
                <pre><code>curl -X POST \<br>-H "Content-Type: application/json" \<br>-H "accept: application/json" \<br>-d '{"descripcion":"Sociedad"}' \<br>http://localhost:8080/api/temasnivel1</code></pre>
                 
                 <p>En caso de que la solicitud sea procesada exitosamente</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;successful&lt;/type&gt;<br />  &lt;description&gt;exito en la operacion&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />description": "exito en la operacion"<br />}</code></pre>

                <p>Si existe algun error de sintaxis, agregando espacios de más o poniendo un XML o JSON incorrecto manda un error 400.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;XMLSyntax&lt;/type&gt;<br />  &lt;description&gt; :Los parametros no son los correctos verificar &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "JSONSyntax",<br />  "description": "Los parametros no son los correctos verificar"<br />}</code></pre>

                <p>En cambio si el error es por parte del servidor muestra un error 500.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;errorServer&lt;/type&gt;<br />  &lt;description&gt; :could not execute statement &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "errorServer",<br />  "description": "could not execute statement"<br />}</code></pre>


                <h3>PUT</h3>
                <p>Para modificar algun Tema existente, se realiza la solicitud a la siguiente URL mostrada en el recuadro, indicando el ID de el tema a modificar. Se pueden enviar formatos XML o JSON.</p>

                <p>Peticion XML</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel1/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel1/{id}</code></pre>

                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning.</p>

                 <h3>DELETE</h3>
                <p>Para eliminar un tema se indica su ID, es muy similar a la solicitud GET. Para esta solo ponemos las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>,</p>

                <p>Peticion XML</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel1/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel1/{id}</code></pre>

                <p>En caso de se haya borrado el dato, se regresa un mensaje de éxito. De lo contrario un mensaje de warning</p>

                <pre><code>XML Exito<br>&lt;message&gt;<br>  &lt;type&gt;successful&lt;/type&gt;<br>  &lt;description&gt;exito en la operacion&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Exito<br>{<br>  "type": "successful",<br>  "description": "exito en la operacion"<br>}<br><br>XML Warning<br>&lt;message&gt;<br>  &lt;type&gt;Warning&lt;/type&gt;<br>  &lt;description&gt;No existe el elemeto solicitado con id:{id}&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Warning<br>{<br>  "type": "Warning",<br>  "description": "No existe el elemeto solicitado con id:{id}"<br>}</code></pre>


                <!--TEMAS 2-->
                <!--TEMAS 2-->
                <a id="Temas2"></a><h2>Temas de Nivel 2</h2>
                <p>Los "Temas de Nivel 2" son una clasificación aún mas específica de los "Temas de nivel 2" a estos les corresponde un ID, una descripción y el ID del "Tema de nivel 2" al que pertenece.</p>
                
                <h3>GET</h3>
                <p>Al hacer una petición al siguiente recurso devuelve un XML o JSON, según se especifique dentro de las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con la lista de todos los "Temas de Nivel 2" compuesta por el ID, la descripción del tema y el ID del "Tema del nivel 2" al que corresponde.</p>
                <p>Petición en XML:</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/temasnivel2</code></pre>

                <p>Resultado:</p>

                <pre><code>&lt;list&gt;<br />  &lt;Tema-Nivel-2&gt;<br />    &lt;idtemasnivel2&gt;0&lt;/idtemasnivel2&gt;<br />    &lt;descripcion&gt;Caracteristicas del empleo de la poblacion&lt;/descripcion&gt;<br />    &lt;idTemasNivel2&gt;0&lt;/idTemasNivel2&gt;<br />  &lt;/Tema-Nivel-2&gt;<br />&lt;/list&gt;
                </code></pre>
                
                <p>Petición en JSON:</p>
                <pre><code>curl -X GET \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel2</code></pre>

                <p>Resultado:</p>

                <pre><code>{<br />  &quot;data&quot;: [{<br />    &quot;idtemasnivel2&quot;: 0,<br />    &quot;descripcion&quot;: &quot;Caracteristicas del empleo de la poblacion&quot;,<br />    &quot;idTemasNivel2&quot;: 0<br />  }]<br />}</code></pre>

                <p>Esta solicitud regresara un XML o JSON, según se especifique en las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con el ID, la descripción del tema y el ID del "Tema de nivel 2" al que pertenece. Para obtener un tema de Nivel 3 en específico, se agrega como parametro a la URL su ID.</p>
                <p>Petición en XML:</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/temasnivel2/0</code></pre>

                <p>Resultado:</p>
                <pre><code>&lt;Tema-Nivel-2&gt;<br />  &lt;idtemasnivel2&gt;0&lt;/idtemasnivel2&gt;<br />  &lt;descripcion&gt;Caracteristicas del empleo de la poblacion&lt;/descripcion&gt;<br />  &lt;idTemasNivel2&gt;0&lt;/idTemasNivel2&gt;<br />&lt;/Tema-Nivel-2&gt;</code></pre>

                <p>Petición en JSON</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/temasnivel2/0</code></pre>

                <p>Resultado</p>

                <pre><code>{<br />  &quot;idtemasnivel2&quot;: 0,<br />  &quot;descripcion&quot;: &quot;Caracteristicas del empleo de la poblacion&quot;,<br />  &quot;idTemasNivel2&quot;: 0<br />}</code></pre>

                <p>En caso de que no existan los recursos solicitados, se regresa un mensaje de warning</p>
                
                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;Warning&lt;/type&gt;<br />  &lt;description&gt;No existe el elemeto solicitado con id:3&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  &quot;type&quot;: &quot;Warning&quot;,<br />  &quot;description&quot;: &quot;No existe el elemeto solicitado con id:3&quot;<br />}</code></pre>

                <h3>POST</h3>
                <p>Para registrar un nuevo tema, se da el nombre del nuevo Tema y el ID del tema de nivel 2 al que corresponde, no es necesario ingresar el ID del tema que se está ingresando debido a que el servidor le asigna un ID automáticamente, la información se puede enviar en XML o JSON. Las cabeceras pueden ser <code>Accept: application/xml</code>, <code>Content-Type: application/xml</code> ó <code>Accept: application/json</code>,  <code>Content-Type: application/json</code></p>
                
                <p>Petición XML</p>
                <pre><code>curl -X POST \<br />-H "Content-Type: application/xml" \<br />-H "Accept: application/xml" \-d "&lt;temanivel2&gt;&lt;descripcion&gt;Sociedad&lt;/descripcion&gt;&lt;idTemasNivel2&gt;0&lt;/idTemasNivel2&gt;&lt;/temanivel2&gt;" \<br />dominio/api/temasnivel2</code></pre>
                
                <p>Petición JSON</p>
                <pre><code>curl -X POST \<br />-H "Content-Type: application/json" \<br />-H "Accept: application/json" \<br />-d '{"descripcion":"Sociedad","idTemasNivel2":0}' \<br />dominio/api/temasnivel2</code></pre>

                <p>En caso de que la solicitud sea procesada exitosamente</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;successful&lt;/type&gt;<br />  &lt;description&gt;exito en la operacion&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />description": "exito en la operacion"<br />}</code></pre>

                <p>Si existe algun error de sintaxis, agregando espacios de más o poniendo un XML o JSON incorrecto manda un error 400.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;XMLSyntax&lt;/type&gt;<br />  &lt;description&gt; :Los parametros no son los correctos verificar &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "JSONSyntax",<br />  "description": "Los parametros no son los correctos verificar"<br />}</code></pre>

                <p>En cambio si el error es por parte del servidor muestra un error 500.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;errorServer&lt;/type&gt;<br />  &lt;description&gt; :could not execute statement &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "errorServer",<br />  "description": "could not execute statement"<br />}</code></pre>


                <h3>PUT</h3>
                <p>Para modificar algun Tema existente, se realiza la solicitud a la siguiente URL mostrada en el recuadro, indicando el ID de el tema a modificar. Se pueden enviar formatos XML o JSON.</p>

                <p>Peticion XML</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel2/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel2/{id}</code></pre>

                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning.</p>
                
                <h3>DELETE</h3>
                <p>Para eliminar un tema se indica su ID, es muy similar a la solicitud GET. Para esta solo ponemos las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>,</p>

                <p>Peticion XML</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel2/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel2/{id}</code></pre>

                <p>En caso de se haya borrado el dato, se regresa un mensaje de éxito. De lo contrario un mensaje de warning</p>

                <pre><code>XML Exito<br>&lt;message&gt;<br>  &lt;type&gt;successful&lt;/type&gt;<br>  &lt;description&gt;exito en la operacion&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Exito<br>{<br>  "type": "successful",<br>  "description": "exito en la operacion"<br>}<br><br>XML Warning<br>&lt;message&gt;<br>  &lt;type&gt;Warning&lt;/type&gt;<br>  &lt;description&gt;No existe el elemeto solicitado con id:{id}&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Warning<br>{<br>  "type": "Warning",<br>  "description": "No existe el elemeto solicitado con id:{id}"<br>}</code></pre>


                <!--TEMAS 3-->
                <a id="Temas3"></a><h2>Temas de Nivel 3</h2>
                <p>Los "Temas de nivel 3" son una clasificación aún mas específica de los "Temas de nivel 2" a estos les corresponde un ID, una descripción y el ID del "Tema de nivel 2" al que pertenece.</p>
                
                <h3>GET</h3>
                <p>Al hacer una petición al siguiente recurso devuelve un XML o JSON, según se especifique dentro de las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con la lista de todos los "Temas de nivel 3" compuesta por el ID, la descripción del tema y el ID del "Tema del nivel 2" al que corresponde.</p>
                <p>Petición en XML:</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/temasnivel3</code></pre>

                <p>Resultado:</p>

                <pre><code>&lt;list&gt;<br />  &lt;tema-nivel-3&gt;<br />    &lt;idTemasNivel3&gt;0&lt;/idTemasNivel3&gt;<br />    &lt;descripcion&gt;Caracteristicas del empleo de la poblacion&lt;/descripcion&gt;<br />    &lt;idTemasNivel2&gt;0&lt;/idTemasNivel2&gt;<br />  &lt;/tema-nivel-3&gt;<br />&lt;/list&gt;
                </code></pre>
                
                <p>Petición en JSON:</p>
                <pre><code>curl -X GET \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel3</code></pre>

                <p>Resultado:</p>

                <pre><code>{<br />  &quot;data&quot;: [{<br />    &quot;idTemasNivel3&quot;: 0,<br />    &quot;descripcion&quot;: &quot;Caracteristicas del empleo de la poblacion&quot;,<br />    &quot;idTemasNivel2&quot;: 0<br />  }]<br />}</code></pre>

                <p>Esta solicitud regresara un XML o JSON, según se especifique en las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con el ID, la descripción del tema y el ID del "Tema de nivel 2" al que pertenece. Para obtener un tema de Nivel 3 en específico, se agrega como parametro a la URL su ID.</p>
                <p>Petición en XML:</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/temasnivel3/0</code></pre>

                <p>Resultado:</p>
                <pre><code>&lt;Tema-Nivel-3&gt;<br />  &lt;idTemasNivel3&gt;0&lt;/idTemasNivel3&gt;<br />  &lt;descripcion&gt;Caracteristicas del empleo de la poblacion&lt;/descripcion&gt;<br />  &lt;idTemasNivel2&gt;0&lt;/idTemasNivel2&gt;<br />&lt;/Tema-Nivel-3&gt;</code></pre>

                <p>Petición en JSON</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/temasnivel3/0</code></pre>

                <p>Resultado</p>

                <pre><code>{<br />  &quot;idTemasNivel3&quot;: 0,<br />  &quot;descripcion&quot;: &quot;Caracteristicas del empleo de la poblacion&quot;,<br />  &quot;idTemasNivel2&quot;: 0<br />}</code></pre>

                <p>En caso de que no existan los recursos solicitados, se regresa un mensaje de warning</p>
                
                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;Warning&lt;/type&gt;<br />  &lt;description&gt;No existe el elemeto solicitado con id:3&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  &quot;type&quot;: &quot;Warning&quot;,<br />  &quot;description&quot;: &quot;No existe el elemeto solicitado con id:3&quot;<br />}</code></pre>

                <h3>POST</h3>
                <p>Para registrar un nuevo tema, se da el nombre del nuevo Tema y el ID del tema de nivel 2 al que corresponde, no es necesario ingresar el ID del tema que se está ingresando debido a que el servidor le asigna un ID automáticamente, la información se puede enviar en XML o JSON. Las cabeceras pueden ser <code>Accept: application/xml</code>, <code>Content-Type: application/xml</code> ó <code>Accept: application/json</code>,  <code>Content-Type: application/json</code></p>
                
                <p>Petición XML</p>
                <pre><code>curl -X POST \<br />-H "Content-Type: application/xml" \<br />-H "Accept: application/xml" \-d "&lt;temanivel3&gt;&lt;descripcion&gt;Sociedad&lt;/descripcion&gt;&lt;idTemasNivel2&gt;0&lt;/idTemasNivel2&gt;&lt;/temanivel3&gt;" \<br />dominio/api/temasnivel3</code></pre>
                
                <p>Petición JSON</p>
                <pre><code>curl -X POST \<br />-H "Content-Type: application/json" \<br />-H "Accept: application/json" \<br />-d '{"descripcion":"Sociedad","idTemasNivel2":0}' \<br />dominio/api/temasnivel3</code></pre>

                <p>En caso de que la solicitud sea procesada exitosamente</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;successful&lt;/type&gt;<br />  &lt;description&gt;exito en la operacion&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />description": "exito en la operacion"<br />}</code></pre>

                <p>Si existe algun error de sintaxis, agregando espacios de más o poniendo un XML o JSON incorrecto manda un error 400.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;XMLSyntax&lt;/type&gt;<br />  &lt;description&gt; :Los parametros no son los correctos verificar &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "JSONSyntax",<br />  "description": "Los parametros no son los correctos verificar"<br />}</code></pre>

                <p>En cambio si el error es por parte del servidor muestra un error 500.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;errorServer&lt;/type&gt;<br />  &lt;description&gt; :could not execute statement &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "errorServer",<br />  "description": "could not execute statement"<br />}</code></pre>


                <h3>PUT</h3>
                <p>Para modificar algun Tema existente, se realiza la solicitud a la siguiente URL mostrada en el recuadro, indicando el ID de el tema a modificar. Se pueden enviar formatos XML o JSON.</p>

                <p>Peticion XML</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel3/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel3/{id}</code></pre>

                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning.</p>
                
                <h3>DELETE</h3>
                <p>Para eliminar un tema se indica su ID, es muy similar a la solicitud GET. Para esta solo ponemos las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>,</p>

                <p>Peticion XML</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel3/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel3/{id}</code></pre>

                <p>En caso de se haya borrado el dato, se regresa un mensaje de éxito. De lo contrario un mensaje de warning</p>

                <pre><code>XML Exito<br>&lt;message&gt;<br>  &lt;type&gt;successful&lt;/type&gt;<br>  &lt;description&gt;exito en la operacion&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Exito<br>{<br>  "type": "successful",<br>  "description": "exito en la operacion"<br>}<br><br>XML Warning<br>&lt;message&gt;<br>  &lt;type&gt;Warning&lt;/type&gt;<br>  &lt;description&gt;No existe el elemeto solicitado con id:{id}&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Warning<br>{<br>  "type": "Warning",<br>  "description": "No existe el elemeto solicitado con id:{id}"<br>}</code></pre>

                
                <!--INDICADORES-->
                <a id="Indicadores"></a><h2>Indicadores</h2>
                <p>Los "Indicadores" son las clasificaciones pertenecientes a los "Temas de nivel 3" que son los que contienen la información de los conteos realizados por año y sus respectivas notas. Los "Indicadores" se componen de un ID, una descripción, su nota correspondiente y el ID "Tema de nivel 3" al que pertenece</p>

                <h3>GET</h3>
                <p>Al hacer una petición al siguiente recurso devuelve un XML o JSON, según se especifique dentro de las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con la lista de todos los "Indicadores" compuesta por el ID, su descripción y el ID del "Tema del nivel 3" al que corresponde.</p>

                <p>Petición XML</p>
                <pre><code>curl -X GET \<br>-H "accept: application/xml" \<br>dominio   /api/indicadores</code></pre>

                <p>Resultado XML</p>
                <pre><code>&lt;list&gt;<br>  &lt;indicador&gt;<br>    &lt;idIndicador&gt;1007000018&lt;/idIndicador&gt;<br>    &lt;descripcion&gt;Poblacion de 14 y mas anios&lt;/descripcion&gt;<br>    &lt;id_temas_nivel_3&gt;0&lt;/id_temas_nivel_3&gt;<br>  &lt;/indicador&gt;<br>  &lt;indicador&gt;<br>    &lt;idIndicador&gt;1007000019&lt;/idIndicador&gt;<br>    &lt;descripcion&gt;Poblacion economicamente activa&lt;/descripcion&gt;<br>    &lt;id_temas_nivel_3&gt;0&lt;/id_temas_nivel_3&gt;<br>  &lt;/indicador&gt;<br>&lt;/list&gt;</code></pre>

                <p>Petición JSON</p>
                <pre><code>curl -X GET \<br>-H "accept: application/json" \<br>dominio/api/indicadores</code></pre>

                <p>Resultado JSON</p>
                <pre><code>{<br>  "data": [{<br>    "idIndicador": "1007000018",<br>    "descripcion": "Poblacion de 14 y mas anios",<br>    "id_temas_nivel_3": 0<br>  }, {<br>    "idIndicador": "1007000019",<br>    "descripcion": "Poblacion economicamente activa",<br>    "id_temas_nivel_3": 0<br>  }]<br>}</code></pre>

                <p>Esta solicitud regresara un XML o JSON, según se especifique en las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>, con el ID, la descripción del identificador y el ID del "Tema de nivel 3" al que pertenece. Para obtener un tema de Nivel 3 en específico, se agrega como parametro a la URL su ID.</p>
                <p>Cabecera  en XML:</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/indicadores/1007000018</code></pre>

                <p>Resultado:</p>
                <pre><code>&lt;indicador&gt;<br />  &lt;idIndicador&gt;1007000018&lt;/idIndicador&gt;<br />  &lt;descripcion&gt;Poblacion de 14 o mas anios&lt;/descripcion&gt;<br />  &lt;nota&gt;Datos ajustados a los montos poblacionales de las proyecciones demograficas del CONAPO&lt;/nota&gt;<br />  &lt;id__temas__nivel__3&gt;0&lt;/id__temas__nivel__3&gt;<br />&lt;/indicador&gt;</code></pre>

                <p>Petición en JSON</p>

                <pre><code>curl -X GET \<br>-H "Accept: application/xml" \<br>dominio/api/indicadores/1007000018</code></pre>

                <p>Resultado</p>

                <pre><code>{<br>  "idIndicador": "1007000018",<br>  "descripcion": "Poblacion de 14 o mas anios",<br>  "nota": "Datos ajustados a los montos poblacionales de las proyecciones demograficas del CONAPO",<br>  "id_temas_nivel_3": 0<br>}</code></pre>

                <p>En caso de que no existan los recursos solicitados, se regresa un mensaje de warning</p>
                
                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;Warning&lt;/type&gt;<br />  &lt;description&gt;No existe el elemeto solicitado con id:3&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  &quot;type&quot;: &quot;Warning&quot;,<br />  &quot;description&quot;: &quot;No existe el elemeto solicitado con id:3&quot;<br />}</code></pre>


                <h3>POST</h3>
                <p>Para registrar un nuevo indicador es necesario ingresar el ID del indicador que se está ingresando debido a que el servidor no le asigna un ID automáticamente, se da el nombre del nuevo indicador, su nota y el ID del tema de nivel 3 al que corresponde, la información se puede enviar en XML o JSON. Las cabeceras pueden ser <code>Accept: application/xml</code>, <code>Content-Type: application/xml</code> ó <code>Accept: application/json</code>, <code>Content-Type: application/json</code></p>

                <p>Petición XML</p>
                <pre><code>curl -X POST<br />-H &quot;Content-Type: application/xml&quot;<br />-H &quot;accept: application/xml&quot;<br />-d &quot;&lt;indicador&gt;<br />&lt;idIndicador&gt;9988232&lt;/idIndicador&gt;<br />&lt;descripcion&gt;mayores de edad&lt;/descripcion&gt;<br />&lt;nota&gt;alguna descripcion&lt;/nota&gt;<br />&lt;/indicador&gt;&quot;<br />http://localhost:8080/api/indicadores</code></pre>

                <p>Petición JSON</p>
                <pre><code>curl -X POST \<br />-H "Content-Type: application/json" \<br />-H "accept: application/json" \<br />-d '{"idIndicador":"7009222212","descripcion":"poblacion menor de 14 anios","nota":"nota sobre el nuevo indicador",idTemaNivel3:0}' \<br />http://localhost:8080/api/indicadores</code></pre>

                <p>En caso de que la solicitud sea procesada exitosamente</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;successful&lt;/type&gt;<br />  &lt;description&gt;exito en la operacion&lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />description": "exito en la operacion"<br />}</code></pre>

                <p>Si existe algun error de sintaxis, agregando espacios de más o poniendo un XML o JSON incorrecto manda un error 400.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;XMLSyntax&lt;/type&gt;<br />  &lt;description&gt; :Los parametros no son los correctos verificar &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "JSONSyntax",<br />  "description": "Los parametros no son los correctos verificar"<br />}</code></pre>


                <p>En cambio si el error es por parte del servidor muestra un error 500.</p>

                <pre><code>XML<br />&lt;message&gt;<br />  &lt;type&gt;errorServer&lt;/type&gt;<br />  &lt;description&gt; :could not execute statement &lt;/description&gt;<br />&lt;/message&gt;<br /><br />JSON<br />{<br />  "type": "errorServer",<br />  "description": "could not execute statement"<br />}</code></pre>


                <h3>PUT</h3>
                <p>Para modificar algún indicador existente, se realiza la solicitud a la siguiente URL mostrada en el recuadro, indicando el ID de el tema a modificar. Se pueden enviar formatos XML o JSON.</p>

                <p>Peticion XML</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/xml" \<br />dominio/api/temasnivel3/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X PUT \<br />-H "Accept: application/json" \<br />dominio/api/temasnivel3/{id}</code></pre>
                
                <p>En caso de éxito se regresa un mensaje de éxito. De lo contrario un mensaje de warning.</p>


                <h3>DELETE</h3>
                <p>Para eliminar un indicador se indica su ID, es muy similar a la solicitud GET. Para esta solo ponemos las cabeceras <code>Accept: application/xml</code> ó <code>Accept: application/json</code>,</p>

                <p>Peticion XML</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/xml" \<br />dominio/api/indicadores/{id}</code></pre>

                <p>Peticion JSON</p>

                <pre><code>curl -X DELETE \<br />-H "Accept: application/json" \<br />dominio/api/indicadores/{id}</code></pre>

                <p>En caso de se haya borrado el dato, se regresa un mensaje de éxito. De lo contrario un mensaje de warning</p>

                <pre><code>XML Exito<br>&lt;message&gt;<br>  &lt;type&gt;successful&lt;/type&gt;<br>  &lt;description&gt;exito en la operacion&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Exito<br>{<br>  "type": "successful",<br>  "description": "exito en la operacion"<br>}<br><br>XML Warning<br>&lt;message&gt;<br>  &lt;type&gt;Warning&lt;/type&gt;<br>  &lt;description&gt;No existe el elemeto solicitado con id:{id}&lt;/description&gt;<br>&lt;/message&gt;<br><br>JSON Warning<br>{<br>  "type": "Warning",<br>  "description": "No existe el elemeto solicitado con id:{id}"<br>}</code></pre>















                <h2 class="blog-post-title" id="Cabecerasresp">Cabeceras de respuesta</h2>

                <a id="Cabget"></a><h2>GET</h2>

                <pre><code>XML<br />Status Code: 200 OK<br />Content-Length: 173<br />Content-Type: application/xml<br />Date: Fri, 08 Aug 2014 03:09:41 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON<br />Status Code: 200 OK<br />Content-Length: 97<br />Content-Type: application/json<br />Date: Fri, 08 Aug 2014 03:11:39 GMT<br />Server: Apache-Coyote/1.1<br /><br />XML Error<br />Status Code: 404 No Encontrado<br />Content-Length: 113<br />Content-Type: application/xml<br />Date: Thu, 07 Aug 2014 23:46:33 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON Error<br />Status Code: 404 No Encontrado<br />Content-Length: 75<br />Content-Type: application/json<br />Date: Thu, 07 Aug 2014 23:38:28 GMT<br />Server: Apache-Coyote/1.1</code></pre>

                <a id="Cabpost"></a><h2>POST</h2>

                <pre><code>XML<br />Status Code: 200 OK<br />Content-Length: 75<br />Content-Type: application/xml<br />Date: Thu, 07 Aug 2014 23:38:28 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON<br />Status Code: 200 OK<br />Content-Length: 75<br />Content-Type: application/json<br />Date: Thu, 07 Aug 2014 23:38:28 GMT<br />Server: Apache-Coyote/1.1<br /><br />XML Error 400<br />Status Code: 400 Petición incorrecta<br />Connection: close<br />Content-Length: 98<br />Content-Type: application/xml<br />Date: Fri, 08 Aug 2014 04:45:50 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON Error 400<br />Status Code: 400 Petición incorrecta<br />Connection: close<br />Content-Length: 98<br />Content-Type: application/json<br />Date: Fri, 08 Aug 2014 04:45:50 GMT<br />Server: Apache-Coyote/1.1<br /><br />XML Error 500<br />Status Code: 500 Error Interno del Servidor<br />Connection: close<br />Content-Length: 68<br />Content-Type: application/xml<br />Date: Fri, 08 Aug 2014 04:58:25 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON Error 500<br />Status Code: 500 Error Interno del Servidor<br />Connection: close<br />Content-Length: 68<br />Content-Type: application/json<br />Date: Fri, 08 Aug 2014 04:58:25 GMT<br />Server: Apache-Coyote/1.1</code></pre>
               



                <a id="Cabdel"></a><h2>DELETE</h2>

                <pre><code>XML<br />Status Code: 200 OK<br />Content-Length: 173<br />Content-Type: application/xml<br />Date: Fri, 08 Aug 2014 03:09:41 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON<br />Status Code: 200 OK<br />Content-Length: 97<br />Content-Type: application/json<br />Date: Fri, 08 Aug 2014 03:11:39 GMT<br />Server: Apache-Coyote/1.1<br /><br />XML Error<br />Status Code: 404 No Encontrado<br />Content-Length: 113<br />Content-Type: application/xml<br />Date: Thu, 07 Aug 2014 23:46:33 GMT<br />Server: Apache-Coyote/1.1<br /><br />JSON Error<br />Status Code: 404 No Encontrado<br />Content-Length: 75<br />Content-Type: application/json<br />Date: Thu, 07 Aug 2014 23:38:28 GMT<br />Server: Apache-Coyote/1.1</code></pre>

              </div><!-- /.blog-post -->
              
            </div><!-- /.blog-main -->

            <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
              <div class="sidebar-module">
                <h4>Temas</h4>
                <ul class="list-unstyled">
                    <li>
                        <a href="#Recursos">Recursos</a>
                        <ul>
                            <li><a href="#Entidades">Entidades</a></li>
                            <li><a href="#Municipios">Municipios</a></li>
                            <li><a href="#Temas1">Temas 1</a></li>
                            <li><a href="#Temas2">Temas 2</a></li>
                            <li><a href="#Temas3">Temas 3</a></li>
                            <li><a href="#Indicadores">Indicadores</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#Cabecerasresp">Cabeceras de respuesta</a>
                        <ul>
                            <li><a href="#Cabget">GET</a></li>
                            <li><a href="#Cabpost">POST</a></li>
                            <li><a href="#Cabdel">DELETE</a></li>
                        </ul>
                    </li>
                  </ul>
              </div>
            
            </div><!-- /.blog-sidebar -->

          </div><!-- /.row -->

        </div><!-- /.container -->

        <div class="blog-footer">
          <p>API INEGI</p>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->




        <div id='IrArriba'>
          <a href='#Arriba'><span/></a>
        </div>
        <script type='text/javascript'>
          //<![CDATA[
          // Botón para Ir Arriba
          jQuery.noConflict();
          jQuery(document).ready(function() {
            jQuery("#IrArriba").hide();
            jQuery(function () {
              jQuery(window).scroll(function () {
                if (jQuery(this).scrollTop() > 100) {
                  jQuery('#IrArriba').fadeIn();
                } else {
                  jQuery('#IrArriba').fadeOut();
                }
              });
              jQuery('#IrArriba a').click(function () {
                jQuery('body,html').animate({
                  scrollTop: 0
                }, 800);
                return false;
              });
            });
          });
          //]]>
        </script>

    </body>
</html>